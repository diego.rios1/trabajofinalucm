# Proyecto Final
### Materia: Desarrollo Para Pentesting 
### Profesor: Cesar Mañoz Araya
### Programa: Especializacion en Ciberseguridad
### Universidad Catolica de Manizales - UCM (Colombia)

Este Repositorio de GitLab se crea para exponer de manera academica un proyecto de la materia **Desarrollo para Pentesting** de la Universidad Católica de Manizales - **UCM**.

#### Descripcion General del proyecto:

Durante la ejecucion del proyecto, se simula un atacante darrollando la tecnica de ataque conocida como **Hombre en el Medio** (en inglés, *Man in the Middle attack*, MitM).


**MitM**
 
Man-in-the-Middle (MitM), que en español significa “hombre en el medio”, es un tipo de ataque destinado a interceptar, sin autorización, la comunicación entre dos dispositivos (hosts) conectados a una red. Este ataque le permite a un agente malintencionado manipular el tráfico interceptado de diferentes formas, ya sea para escuchar la comunicación y obtener información sensible, como credenciales de acceso, información financiera, etc., o para suplantar la identidad de alguna de las partes. Para que un ataque MitM funcione correctamente, el delincuente debe asegurarse que será el único punto de comunicación entre los dos dispositivos, es decir, el delincuente debe estar presente en la misma red que los hosts apuntados en el ataque para cambiar la tabla de enrutamiento para cada uno de ellos.

**Escenario**

- Conexion a Internet
- Equipo de cómputo con SO Windows11 
- Hypervisor Vmware WorkStation v 16.2.3
- Python v.3.10.5
- Máquina Virtual Kali Linux (ATACANTE)
- Máquina Virtual Windows 7 (VICTIMA)

Tanto la máquina del atancante como la máquina de la victima se hace necesario que se encuentren en el mismo segmento de red. 

##### Esquema

![Esquema general del ataque MitM](DiagramaAtaque.png)

En el esquema anterior, se muestra cómo la máquina atacante ejecuta en su orden, dos archivos desarrollados en python, arp_spoofing_end.py y password_sniffer.py, comprometiendo la máquina victima, con lo cual espera (1) modificar la tabla arp y (2) capturar las credenciales del sistema de informacion al cual el usuario de la máquina victima ingresará.